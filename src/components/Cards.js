import React from 'react';
import CardItem from './CardItem';
import './Cards.css';

const Cards = () => {
  return (
    <div className="cards">
      <h1>Check out these EPIC destinations!</h1>
      <div className="cards__container">
        <div className="cards__wrapper">
          <ul className="cards__items">
            <CardItem src="images/img-1.jpg" text="Explore the space and go to the LalaLand" label="Adventure" path="/services"/>
            <CardItem src="images/travel-4.jpg" text="How about a 3 days trips to Singapore?" label="Singapore" path="/services"/>
          </ul>
          <ul className="cards__items">
            <CardItem src="images/travel-1.jpg" text="Have you ever been to Japan?" label="Japan" path="/services"/>
            <CardItem src="images/travel-2.jpg" text="Do you want to see some best deals to Moscow?" label="Moscow" path="/services"/>
            <CardItem src="images/travel-3.jpg" text="Do you want to see the highest skyscrapper in the world? " label="Dubai" path="/services"/>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default Cards
