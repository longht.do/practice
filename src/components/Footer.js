import React from 'react'
import { Button } from './Button'
import './Footer.css'
import { Link } from 'react-router-dom'

const Footer = () => {
  return (
    <div className="footer-container">
      <section className="footer-subscription">
        <p className="footer-subscription-heading">
          Join the Adventure newsletter to receive our best vacation deals
        </p>
        <p className="footer-subcription-text">
          You can unsubcribe at any time.
        </p>
        <div className="input-ares">
          <form>
            <input type="email" name="email" placeholder="Your Email" className="footer-input"/>
            <Button buttonStyle="btn--outline">Subcribe</Button>
          </form>
        </div>
      </section>
      <div className="footer-links">
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>About us</h2>
            <Link to='/sign-up'>How it works</Link>
            <Link to='/'>Testimonials</Link>
            <Link to='/'>Careers</Link>
            <Link to='/'>Investors</Link>
            <Link to='/'>Terms of Services</Link>
          </div>
        </div>
        <div className="footer-link-wrapper">
          <div className="footer-link-items">
            <h2>Contact us</h2>
            <Link to='/sign-up'>How it works</Link>
            <Link to='/'>Testimonials</Link>
            <Link to='/'>Careers</Link>
            <Link to='/'>Investors</Link>
            <Link to='/'>Terms of Services</Link>
          </div>
        </div>
      </div>
      <section className="social-media">
        <div className="social-media-wrap">
          <div className="footer-logo">
            <Link to='/'className="social-logo">
              DoDo <i className="fab fa-typo3" />
            </Link>
          </div>
          <small className="website-rights">Dodo 2021</small>
          <div className="social-icons">
            <Link className="social-icons-link facebook" to='/' target='_blank' aria-label='Facebook'>
              <i className="fab fa-facebook-f"></i>
            </Link><Link className="social-icons-link instagram" to='/' target='_blank' aria-label='Instagram'>
              <i className="fab fa-instagram"></i>
            </Link>
            <Link className="social-icons-link twitter" to='/' target='_blank' aria-label='Twitter'>
              <i className="fab fa-twitter"></i>
            </Link>
            <Link className="social-icons-link linkedin" to='/' target='_blank' aria-label='LinkedIn'>
              <i className="fab fa-linkedin"></i>
            </Link>
          </div>
        </div>
      </section>
    </div>
  )
}

export default Footer
